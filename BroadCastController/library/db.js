const {database} = require('../config')
const schedule = database.GetScheduleTable()
const ingest = database.GetIngestTable()

module.exports = {
    findById: async (input_id)=>{
        return ingest.findAll({ where:{id:input_id} }).then( (error,results) =>{
            if(error) return error
            else return results
        })
    },

}