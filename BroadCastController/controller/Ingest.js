const { database } = require('../config')
const ingest = database.GetIngestTable()
module.exports = async (request, response) => {
	
	const REQUEST_TYPE = request.method   // RESPONSE THE STATUS CODE AND OBJ WHEN PUT OR POST
	if (REQUEST_TYPE === 'GET') {
		if(request.params.id===undefined) {
			ingest.findAll()
			.then(results => {
				response.status(200).send(results)
			})
			.catch(err=>{
				response.status(404).send(err)
			})
		}else {
			ingest.findByPk(request.params.id)
			.then(ing => {
				response.status(200).send(ing)
			})
			.catch(err=>{
				response.status(404).send(err)
			})
		}
	
	} else if (REQUEST_TYPE === 'POST') {
		ing = request.body;
		ingest.create(ing)
		.then(ing=>{
			response.status(201).send(ing);
		})
		.catch(err=>{
			response.status(500).send(err);
			
		})

	} else if (REQUEST_TYPE === 'PUT') {
		ingest.update(request.body,{where: {id: request.params.id}})
		.then(noAffectedRow =>{
			ingest.findByPk(request.params.id).then(updateIng=>{
				response.status(201).send(updateIng)
			})
		})
		.catch(err=>{
			response.status(500).send(err)
		})
	} else if (REQUEST_TYPE === 'DELETE') { 
		ingest.destroy({where:{id:request.params.id}})
		.then(ing=>{
			response.status(200).send(ing)
		})
		.catch(err=>{
			response.status(404).send(err)
		})
		
	}
}