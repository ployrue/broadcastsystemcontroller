const ScheduleController = require('./Schedule')
const IngestController = require('./Ingest')

module.exports = {
    ScheduleController, IngestController
}