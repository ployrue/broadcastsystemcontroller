const { database, constant } = require('../config')
const moment = require('moment-timezone')
const { scheduleService } = require('../services')
const schedule = database.GetScheduleTable()
const ingest= database.GetIngestTable()

module.exports = async (request, response) => {
	const REQUEST_TYPE = request.method
	const REQUEST_PATH = request.path
  	if (REQUEST_TYPE === 'GET') {
		
		console.log('request.params.id=== ' + request.params.id)

		if(request.params.id===undefined){
			var allsch = []
			let previousCheckTime = request.query.startDate
			let checkTime = request.query.endDate

			schedule.findAll({include: [{model:ingest}] })
			.then(results=>{
				if(previousCheckTime!= undefined && checkTime!= undefined){ // want now schedule
					previousCheckTime = moment(previousCheckTime)
					checkTime = moment(checkTime)
					for(i=0;i<results.length;i++){
						
							const startTime = moment(results[i].startTime)
							const endTime = moment(results[i].endTime)

							if(startTime>=(previousCheckTime) && startTime<=(checkTime) && endTime>=(checkTime)){
								allsch.push(results[i])
							}else if(startTime>=(previousCheckTime) && endTime<=(checkTime)){
								allsch.push(results[i])
							}else if(endTime>=(previousCheckTime) && endTime<=(checkTime) &&  startTime<=(previousCheckTime)){
								allsch.push(results[i])
							}else if(startTime<=(previousCheckTime) && endTime>=(checkTime)){
								allsch.push(results[i])
							}	
					}
					response.status(200).send(allsch)
			
				}else{ //only want all
					response.status(200).send(results)	
				}
			})
			.catch(err=>{
				response.status(404).send(err)	
			})
		}else{
			schedule.findByPk(request.params.id,{include: [ { model:ingest} ] })
			.then(sch => {	
				response.status(200).send(sch)
			})
			.catch(err=>{
				response.status(404).send({ status: 'fail', message: err })
			})
		}

  	}else if (REQUEST_TYPE === 'POST') {
		sch = request.body;
		var responseSch = []
		if (sch.startTime === undefined){
			const currentTime = moment()
			sch.startTime = currentTime.format(constant.TIME_FORMAT)
			sch.endTime = currentTime.add(moment.duration(1, 'm')).format(constant.TIME_FORMAT) // add 1 hour
		}
		if(REQUEST_PATH==='/schedule/crash/' || REQUEST_PATH==='/schedule/crash') { //create now
			sch.isRecurring = false //set isRecurring = false
		}

		scheduleService.isTimeOverlaping(sch,'startTime').then(startTimeOverlapingSch => {
			var create = true
			if(startTimeOverlapingSch===null) // have not startTime Overlaping
			{	console.log('NOT startTime Overlaping')
				scheduleService.isOverlaping(sch).then(editEndTimeSch=>{ //find EndTime Overlaping

					if(editEndTimeSch!==null) //have endTime Overlaping
					{
						console.log('EndTime Overlaping----------------------')
						console.log('NEW endTime==='+ editEndTimeSch.endTime)
						// let newEndTimeOverlaping = scheduleService.isTimeOverlaping(editEndTimeSch,'endTime')
						//scheduleService.isTimeOverlaping(editEndTimeSch,'endTime')
						//.then(newEndTimeOverlaping=>{
						//console.log(newEndTimeOverlaping)
						if(editEndTimeSch.endTime<=sch.startTime) {  

							create = false
							response.status(403).send(editEndTimeSch)

						}else sch.endTime = editEndTimeSch.endTime 
						//})
						

					}else {console.log('NOT EndTime Overlaping')}
					
					if(create){
						schedule.create(sch)
						.then(newSch=>{
							sch.parentScheduleId = newSch.id
							responseSch.push(newSch)
							if(sch.isRecurring){
								let allsch = scheduleService.createAllSchedule(sch)
								for(i=0;i<allsch.length;i++){
									schedule.create(allsch[i])
									.then(newRecuringSch=>{
										responseSch.push(newRecuringSch)
										if(responseSch.length===allsch.length+1) response.status(201).send(responseSch)
		
									})
									.catch(err=>{
										console.log('err')
										response.status(500).send(err)
									})
								} 
							} else {
								response.status(201).send(newSch)
							}
						})
						.catch(err=>{
							response.status(500).send(err)
						})
					}
				})
			}else
			{
				console.log('startTime Overlaping')
				response.status(403).send(startTimeOverlapingSch)
			}
		})

	}else if (REQUEST_TYPE === 'PUT') {
		schedule.update(request.body,{where:{id:request.params.id}})
		.then(noAffectedRow =>{
			schedule.findByPk(request.params.id).then(updateSch=>{
				response.status(201).send(updateSch)
			})
		})
		.catch(err=>{
			response.status(404).send(err)
		})	

	} else if (REQUEST_TYPE === 'DELETE') {

		schedule.destroy({where:{id:request.params.id}})
		.then(sch=>{
			response.status(200).send(sch)
		})
		.catch(err=>{
			response.status(404).send(err)
		})
	}
}