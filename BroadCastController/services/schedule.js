const { database, constant } = require('../config')
const schedule = database.GetScheduleTable()
const orm = require('orm')
const moment = require('moment')
const sequelize = require('sequelize')
const op = sequelize.Op

module.exports = {
    createAllSchedule: (sch)=>{
        let allSch = []
            let activeDays = [];
            for (i=0;i<7;i++){
                if (sch.activeDays.length > i) 
                    activeDays[i] = sch.activeDays.charAt(i)==1
                else
                    activeDays[i] = false;
            }
            if (sch.validUntil === null)
                validUntil = moment(sch.startTime).add(moment.duration(10, 'years'));
            else
                validUntil = validUntil = moment(sch.validUntil);

            currentStartTime= moment(sch.startTime).add(moment.duration(1, 'day'))
            currentEndTime = moment(sch.endTime).add(moment.duration(1, 'day'))

            while (currentStartTime < validUntil){ 
                dayofWeek = moment(currentStartTime).day()
                if (activeDays[dayofWeek]){
                    //create the schedule object and push to the array
                    allSch.push(
                        {
                            fileName: sch.fileName,
                            title: sch.title,
                            validUntil: validUntil.format(constant.TIME_FORMAT),
                            startTime: currentStartTime.format(constant.TIME_FORMAT),
                            endTime: currentEndTime.format(constant.TIME_FORMAT),
                            activeDays: sch.activeDays, //0101010 Sun-Sat
                            status: sch.status,     // Pending:0, In preparation:1, In process:2, Done:3, Deleted:4, Error:5 
                            isRecurring: sch.isRecurring,
                            actualStartTime: sch.actualStartTime,
                            actualEndTime: sch.actualEndTime,
                            parentScheduleId: sch.parentScheduleId,
                            metadata: sch.metadata,
                            tags: sch.tags,
                            ingestId: sch.ingestId
                        }
                    )
                    console.log(currentStartTime);
                    console.log(currentEndTime);
                }
                currentStartTime = currentStartTime.add(moment.duration(1, 'day'));
                currentEndTime = currentEndTime.add(moment.duration(1, 'day'));

            }
            return allSch         
    }
    ,
    isOverlaping: (sch)=>{
        return new Promise((resolve, reject) => {
            
            const newObjStartTime = moment(sch.startTime).format(constant.TIME_FORMAT)
            const newObjEndTime = moment(sch.endTime).format(constant.TIME_FORMAT)

            schedule.findAll(
                {where:
                {            
                    ingestId:sch.ingestId,
                    [op.or]:[
                        { startTime: {[op.gte]:newObjStartTime,[op.lte]:newObjEndTime} },
                        { endTime: {[op.gte]:newObjStartTime,[op.lte]:newObjEndTime} }
                    ]
                }
            },{order:['startTime']})
            .then(overlapingSch=>{
                if(overlapingSch.length>0)
                {
                        for(i=0;i<overlapingSch.length;i++){
                            const overlapingSchStartTime = moment(overlapingSch[i].startTime).format(constant.TIME_FORMAT)
                            if(overlapingSchStartTime>newObjStartTime){
                                //console.log('satrtTime seletc' + moment(overlapingSch[i].startTime).format(constant.TIME_FORMAT))
                                sch.endTime =  moment(overlapingSch[i].startTime).add(-1,'s').format(constant.TIME_FORMAT)
                                break 
                            }
                        }
                        resolve(sch)  
                } 
                else
                {
                    resolve(null)
                } 
            })
            .catch(err=>{
                reject(err)
            })
        })
    },
    isTimeOverlaping: (sch,timePoint)=>{
        return new Promise((resolve, reject) => {
            let newObjTime
            if(timePoint==='startTime') newObjTime = moment(sch.startTime).format(constant.TIME_FORMAT)
            else if(timePoint==='endTime') newObjTime = moment(sch.endTime).format(constant.TIME_FORMAT)
          
            schedule.findAll({where:{ingestId:sch.ingestId, startTime:{[op.lte]:newObjTime},endTime:{[op.gte]:newObjTime} }})     
            .then(overlapingSch=>{
                console.log(overlapingSch.length)
                console.log('id overlap '+ overlapingSch.id)
                if(overlapingSch.length>0)
                {
                    console.log('HAVE OVERLAPINGNNNNNNNNNNNNNNNNNNNNNN'+overlapingSch)
                    resolve(overlapingSch)
                } 
                else
                {
                    resolve(null)
                } 
            })
            .catch(err=>{
                reject(err)
            })
        })
    },
}