const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const express = require('express');
const app = express();
const bodyParser = require('body-parser')
const {catchAsyncErrors} = require('./utils/catchAsyncErrors')
const router  =  express.Router()
const routeConfig = require('./config/routes')
const  {database} = require('./config')

const cors = require('cors')





app.use(cors())
database.GetIngestTable().sync({alter: true})
database.GetScheduleTable().sync({alter: true})





//process.env.TZ = 'Europe/Istanbul' 
// view engine setup
//app.set('views', path.join(__dirname, 'views'));
//app.set('view engine', 'jade');

app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(bodyParser.json());
// use body parser

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//app.use('/', route);

//app.use('/users', usersRouter);

// catch 404 and forward to error handler
// app.use(function(req, res, next) {
//   next(createError(404));
// });

// error handler
// app.use(function(err, req, res, next) {
//   // set locals, only providing error in development
//   res.locals.message = err.message;
//   res.locals.error = req.app.get('env') === 'development' ? err : {};

//   // render the error page
//   res.status(err.status || 500);
//   res.render('error');
// });

// generating API route
Object.keys(routeConfig).forEach(  (key, index) => {
  routeConfig[key].forEach((route) => {
    const {methods ,path ,controller} = route    
    router[methods.toLowerCase()](`/${key}${path}`, catchAsyncErrors(controller))
  })
})
app.use('/api', router)

  

app.listen(8000 , ()=>{
  startAt = new Date()
  console.log(startAt)
  console.log('app listening on port 8000!')
})

module.exports = app;
