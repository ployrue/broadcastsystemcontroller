module.exports = {
	ORM: {
		HOST: 'localhost',
		USER: 'a',
		PASSWORD: '123456',
		DATABASE: 'controller',
		TABLE_NAME:
		{
			SCHEDULE: 'schedule',
			SCHEDULE_HISTORY: 'scheduleHistory',
			INGEST: 'ingest',
			METADATA: 'metadata'
		},
		PORT: '3306',
		DIALECT: 'mysql'
	},
	TIME_FORMAT: 'YYYY-MM-DD HH:mm:ss.SSSSSS'
}