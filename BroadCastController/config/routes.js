var {ScheduleController, IngestController} = require('../controller')
module.exports = {
    schedule:[
        {
            methods: 'POST',
            path: '/',
            controller: ScheduleController
        } ,  
        {
            methods: 'POST',
            path: '/crash',
            controller: ScheduleController
        } ,    
        {
            methods: 'GET',
            path: '/:id',
            controller: ScheduleController
        },   
        {
            methods: 'GET',
            path: '/',
            controller: ScheduleController
        },   
        {
            methods: 'PUT',
            path: '/:id',
            controller: ScheduleController
        },   
        {
            methods: 'DELETE',
            path: '/:id',
            controller: ScheduleController
        }
    ],
    ingest:[
        {
            methods: 'POST',
            path: '/',
            controller: IngestController
        } ,    
       {
            methods: 'GET',
            path: '/:id',
            controller: IngestController
        },   
        {
            methods: 'GET',
            path: '/',
            controller: IngestController
        },   
        {
            methods: 'PUT',
            path: '/:id',
            controller: IngestController
        },   
        {
            methods: 'DELETE',
            path: '/:id',
            controller: IngestController
        }  
    ],
}