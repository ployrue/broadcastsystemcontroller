let constant, database

if (process.env.NODE_ENV === 'production') {
	constant = require('./constant')
	database = require('./db')

} else if (process.env.NODE_ENV === 'staging') {
	constant = require('./constant')
	database = require('./db')
} else {
	constant = require('./constant')
	database = require('./db')
}
module.exports = {
	constant,
	database
} 