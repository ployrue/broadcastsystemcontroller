const mysql = require('mysql')
const constant = require('./constant')
const sequelize = require('sequelize')

class connection {
    init() {
        return new sequelize(constant.ORM.DATABASE, constant.ORM.USER, constant.ORM.PASSWORD, {
            host: constant.ORM.HOST,
            port: constant.ORM.PORT,
            dialect: constant.ORM.DIALECT,
            dialectOptions: {
                useUTC: false, //for reading from database
                dateStrings: true,
                typeCast: true
            },
            timezone: '+03:00'
        })
    }

    GetScheduleTable(){

        var schedule = this.init().define(constant.ORM.TABLE_NAME.SCHEDULE, { // schedule table 
            id : {  type:sequelize.INTEGER,
                    autoIncrement: true,
                    primaryKey: true,
                }
            ,
            fileName: sequelize.STRING,
            title: sequelize.STRING,
            validUntil: sequelize.DATE(6),
            startTime: sequelize.DATE(6),//
            endTime: sequelize.DATE(6),//
            activeDays: sequelize.STRING, //0101010 Sun-Sat
            status: sequelize.ENUM('Pending', 'In preparation','In process','Done','Error'),     // Pending:0, In preparation:1, In process:2, Done:3, Deleted:4, Error:5 
            isRecurring: sequelize.BOOLEAN,
            actualStartTime: sequelize.DATE(6),
            actualEndTime: sequelize.DATE(6),
            parentScheduleId: sequelize.INTEGER,
            metadata: sequelize.JSON,
            woodyId: sequelize.STRING,
            woodyProfileId: sequelize.STRING,
            woodyProfile: sequelize.STRING,
            tags: sequelize.JSON
        },{
            timestamps: false,
            indexes: [
                {
                    unique: false,
                    fields: ['startTime', 'endTime']
                }
            ]
        
        })
        schedule.belongsTo(this.GetIngestTable())
        return schedule
    }
    
    GetIngestTable(){
        return this.init().define(constant.ORM.TABLE_NAME.INGEST, { // schedule table 
            id : { type:sequelize.INTEGER,autoIncrement: true,primaryKey: true},
            port: sequelize.INTEGER,
            ipAddress: sequelize.STRING,
            portName: sequelize.STRING,
            title: sequelize.STRING,
            playerName: sequelize.STRING,
            sharedPath: sequelize.STRING,
            fileExtension: sequelize.STRING
        },{
            timestamps: false, 
        })
    }
}

module.exports = new connection